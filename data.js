let LIST = [
  {
    id: 1,
    product: 'Arroz',
    type: 'Grains'
  },
  {
    id: 2,
    product: 'Lenteja',
    type: 'Grains'
  },
  {
    id: 3,
    product: 'Salchicha ranchera',
    type: 'Meats'
  }
]
;

module.exports = LIST;