// const http = require('http'); THIS A WAY FROM CREATE API WITHOUT DEPENDENCIES

const express = require('express');
const app = express();
/* const LIST = require('./data'); */
const cors = require('cors');
/* const { request } = require('express'); */

let LISTS = [
  {
    id: 1,
    name: 'Work day',
    date: '2019-06-11T00:00',
    list: [
      {
        product: 'Ola',
        type: 'important',
        check: false
      },
      {
        product: 'ke ac',
        type: 'normal',
        check: false
      },
      {
        product: 'bien',
        type: 'regular',
        check: false
      }
    ]
  },
  {
    id: 2,
    name: 'Monday market list',
    date: '2021-06-11T00:00',
    list: [
      {
        product: 'Maracuya',
        type: 'Fruta',
        check: false
      },
      {
        product: 'Melon',
        type: 'Fruta',
        check: false
      },
      {
        product: 'Pollo - Pechuga',
        type: 'Carnes',
        check: false
      }
    ]
  }
];
app.use(cors());

app.use((resquest, res, next) => {
  next();
});

app.use(express.json());
/* app.use(valute); */


// THIS A CREATE CALLBACK WITHOUT EXPRESS
/* const app = http.createServer((request, res) => {
  res.writeHead(200, { 'content-type': 'application/json' });
  res.end(JSON.stringify(LIST));
}); */

app.get('/api/list', (request, res) => {
  res.json(LISTS);
});

app.get('/api/list/:id', (request, res) => {
  const id = Number(request.params.id);
  const findList = LISTS.find((item) => {
    return item.id === id;
  });

  findList ? res.json(findList) : res.status(404).end();

});

app.delete('/api/list/:id', (request, res) => {

  const id = Number(request.params.id);
  const checkRest = LISTS.filter((deleteItem) => {
    return deleteItem.id != id;
  });

  LISTS = checkRest;

  res.json(LISTS);
  res.status(204).end();

});

app.post('/api/list', (request, res) => {
  const dataRequest = request.body;

  if (!dataRequest || !dataRequest.list) {
    return res.status(400).json({
      error: 'Error to add product in your list market'
    });
  }

  let ids = [];
  let maxId = 0;

  if(LISTS.length != 0) {
    ids = LISTS.map(item => item.id),
    maxId = Math.max(...ids);
  }

  const newList = {
    id: maxId + 1,
    name: 'Work day',
    date: new Date(),
    list: dataRequest.list
  };

  /* const newItemList = {
    id: maxId + 1,
    product: dataRequest.product,
    type: typeof dataRequest.type != 'undefined' ? dataRequest.type : 'Other'
  } */


  LISTS = [...LISTS, newList];

  const dataClient = {
    new: newList,
    list: LISTS
  };

  res.status(201).json(dataClient);
});

app.put('/api/list/:id', (request, res) => {
  const id = Number(request.params.id);
  let dataUpdate = request.body;

  const indexId = LISTS.findIndex(i => i.id === id);
  
  const update = {
    id: id,
    name: dataUpdate.name ? dataUpdate.name : LISTS[indexId].name,
    date: LISTS[indexId].name,
    list: dataUpdate.list ? dataUpdate.list : LISTS[indexId].list
  };

  LISTS = LISTS.map(list => list.id !== update.id  ? list : update);

  res.json(LISTS);

});

app.use((request, res) => {
  res.status(404).json({
    error: 'Not found'
  });
});

const PORT = process.env.PORT || 3001;
app.listen(PORT);
console.log(`start server in a port ${PORT}`);